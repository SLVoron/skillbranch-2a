import express from 'express';

const app = express();
app.get('/', (req, res) => {
	const numbers = {
		a: req.query.a || 0,
		b: req.query.b || 0
	}
	res.send( +numbers.a + +numbers.b + '');
});

app.listen(3000)